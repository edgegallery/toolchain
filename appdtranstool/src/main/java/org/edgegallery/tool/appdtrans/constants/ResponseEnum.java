package org.edgegallery.tool.appdtrans.constants;

import lombok.Getter;

@Getter
public enum ResponseEnum {

    BAD_REQUEST("BAD_REQUEST", 400),
    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR", 500);

    private String name;

    private int code;

    ResponseEnum(String name, int code) {
        this.name = name;
        this.code = code;
    }
}
